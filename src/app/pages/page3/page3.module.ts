import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page3RoutingModule } from './page3-routing.module';
import { Page3Component } from './page3.component';
import { Routes } from '@angular/router';
import { PageComponent } from './page/page.component';

const routes: Routes = [
  {
    path: '',
    component: Page3Component
  }
];

@NgModule({
  declarations: [Page3Component, PageComponent],
  imports: [
    CommonModule,
    Page3RoutingModule
  ]
})
export class Page3Module { }

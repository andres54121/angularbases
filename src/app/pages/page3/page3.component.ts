import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements OnInit {
  mensajes: any;

  constructor( private dataService: DataService) { }

  ngOnInit() {
    this.mensajes = this.dataService.getPages();
  }

  escuchaClick( id: number ){
    console.log('Click en: ', id);
  }
}

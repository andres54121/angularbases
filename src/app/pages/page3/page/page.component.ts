import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  @Input() mensaje: any;
  @Output() clickPage = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.clickPage.emit(this.mensaje.name);
  }

}

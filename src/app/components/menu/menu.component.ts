import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rutas = [
    {
      name: 'PAGE1',
      path: '/page1'
    },
    {
      name: 'PAGE2',
      path: '/page2'
    },
    {
      name: 'PAGE3',
      path: '/page3'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
